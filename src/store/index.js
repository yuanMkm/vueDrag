import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const state = {
    //设置全局访问的state对象
    input_clientX:'',
    input_clientY:'',
    input_label:'请输入文字'
}

const getters = {
    //实时监听state值的变化(最新状态)
    inputClientX(state){//方法名随意，主要是来承载变化input_clientX的值
        return state.input_clientX;
    },
    inoutClientY(state){
        return state.input_clientY;
    },
    inoutLabe(state){
        return state.input_label;
    }
}

const mutations = {
    //定义改变state属性初始值的方法，此属性中的方法都是同步的
    changeInputClientX(state,clientX){
        //自定义改变state初始值的方法，这里面的参数除了state挚爱还可以再传额外的参数(变量或对象)
        state.input_clientX = clientX;
    },
    changeInputClientY(state,clientY){
        state.input_clientY = clientY;
    },
    changeInputLabel(state,label){
        state.input_label = label;
    }     
}

const actions = {
    //actions 中的方法是异步的。异步执行mutations中的方法
    acInputClientX(context,clientX){
        context.commit('changeInputClientX', clientX);
    },
    acInputClientY(context,clientY){
        context.commit('changeInputClientY', clientY);
    },
    acInputLabel(context,label){
        context.commit('changeInputLabel', label);
    } 
}


const store = new Vuex.Store({
    state,
    getters,
    mutations,
    actions
});


export default store;